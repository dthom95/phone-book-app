# README #

### What is this repository for? ###

* This repository is a PhoneBook app.

### How do I get set up? ###

* The code was built using Visual Studio 2017 using Asp.Net Core 2.0 running on .NET 4.6.1.
* Download the code using the command git clone -b develop https://bitbucket.org/dthom95/phone-book-app.
* Create a blank database called PhoneBook and run the sql script file located in the scripts folder.
* Make sure you have installed the typescript sdk for Visual Studio Version 2.6.2. https://www.microsoft.com/en-us/download/details.aspx?id=55258
* Ensure that you have installed node for windows, the LTS version https://nodejs.org/en/download/
* Run npm install in the root of the Web Project to install the dependencies, or just build the solution.
* It will take a while to install all the node dependencies.
* Run the solution and navigate to the phone book menu item.

### Assumptions ###

* I built the solution using the default template for Angular provided in Visual Studio 2017.
* The code used Asp.Net Core 2.0 running on the .NET 4.6.1 framework.
* The reason for not using full .NET Core 2.0 framework is that Entity Framework Core is not recommended for production usage yet by Microsoft.
* The main reason is that it does not support lazy loading, which severely limits the actual build.
* I have therefore selected Entity Framework 6.2 code first which runs on the full .NET Framework.
* Angular 4 was used for the website with restful Web Api services on the backend.
* I have integrated swashbuckle into the solution to automatically document the rest endpoints.
* The api url for the web api services is set in the config.service.ts, you may have to adjust it to your local environment, but you shouldn't have to as there is a launchsettings.json file.
* Navigate to http://localhost:53570/swagger to see the swagger documentation running.
* I have replaced the default dependency injection framework .NET with Autofac, which supports .NET core.
* All dependencies are configured using it.
* All database integration is done using best practices of repository pattern with a unit of work implementation.
* Automapper is used to map from entities to model objects on the front-end.
