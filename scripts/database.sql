USE [PhoneBook]
GO
/****** Object:  User [phonebook]    Script Date: 2/27/2018 8:59:57 PM ******/
CREATE USER [phonebook] FOR LOGIN [phonebook] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [phonebook]
GO
/****** Object:  Table [dbo].[Entry]    Script Date: 2/27/2018 8:59:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[DateModified] [datetime2](7) NULL,
	[PhoneBookId] [int] NULL,
	[RowGuid] [uniqueidentifier] ROWGUIDCOL  NULL,
 CONSTRAINT [PK_Entry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PhoneBook]    Script Date: 2/27/2018 8:59:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhoneBook](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) NULL,
	[DateModified] [datetime2](7) NULL,
	[RowGuid] [uniqueidentifier] ROWGUIDCOL  NULL,
 CONSTRAINT [PK_PhoneBook] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Entry] ADD  CONSTRAINT [DF_Entry_RowGuid]  DEFAULT (newid()) FOR [RowGuid]
GO
ALTER TABLE [dbo].[PhoneBook] ADD  CONSTRAINT [DF_PhoneBook_RowGuid]  DEFAULT (newid()) FOR [RowGuid]
GO
ALTER TABLE [dbo].[Entry]  WITH CHECK ADD  CONSTRAINT [FK_Entry_PhoneBook] FOREIGN KEY([PhoneBookId])
REFERENCES [dbo].[PhoneBook] ([Id])
GO
ALTER TABLE [dbo].[Entry] CHECK CONSTRAINT [FK_Entry_PhoneBook]
GO
