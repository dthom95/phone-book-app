﻿using System;
using System.Collections.Generic;
using PhoneBookApp.Common.Data;
using PhoneBookApp.Data.Entities;

namespace PhoneBookApp.Data.Services
{
    public interface IEntryService
    {
        IList<Entry> GetEntries();
        IList<Entry> GetEntries(int id);
        void CreateEntry(Entry entry);
        void UpdateEntry(Entry entry);
        void DeleteEntry(int id);
        Entry GetEntryById(int id);
    }

    public class EntryService : IEntryService
    {
        private readonly IRepository<Entry> _entryRepository;

        public EntryService(IRepository<Entry> entryRepository)
        {
            _entryRepository = entryRepository;
        }

        public IList<Entry> GetEntries()
        {
            return _entryRepository.GetAll();
        }

        public IList<Entry> GetEntries(int id)
        {
            return _entryRepository.Fetch(a => a.PhoneBookId == id);
        }

        public void CreateEntry(Entry entry)
        {
            entry.DateModified = entry.DateModified ?? DateTime.Now;
            entry.RowGuid = entry.RowGuid ?? Guid.NewGuid();
            _entryRepository.Create(entry);
        }

        public void UpdateEntry(Entry entry)
        {
            entry.DateModified = entry.DateModified ?? DateTime.Now;
            entry.RowGuid = entry.RowGuid ?? Guid.NewGuid();
            _entryRepository.Update(entry);
        }

        public void DeleteEntry(int id)
        {
            _entryRepository.Delete(id);
        }

        public Entry GetEntryById(int id)
        {
            return _entryRepository.Get(id);
        }
    }
}