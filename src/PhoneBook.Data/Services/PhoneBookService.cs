﻿using System;
using System.Collections.Generic;
using System.Linq;
using PhoneBookApp.Common.Data;
using PhoneBookApp.Data.Entities;

namespace PhoneBookApp.Data.Services
{
    public interface IPhoneBookService
    {
        IList<PhoneBook> GetPhoneBooks();
        IList<Entry> GetPhoneBookEntries(int id);
        IList<Entry> FindPhoneBookEntries(int id, string query);
        PhoneBook GetPhoneBookById(int id);
        void CreatePhoneBook(PhoneBook phoneBook);
        void UpdatePhoneBook(PhoneBook phoneBook);
        void DeletePhoneBook(int id);
    }

    public class PhoneBookService : IPhoneBookService
    {
        private readonly IRepository<PhoneBook> _phoneBookRepository;
        private readonly IRepository<Entry> _entryRepository;

        public PhoneBookService(IRepository<PhoneBook> phoneBookRepository, IRepository<Entry> entryRepository)
        {
            _phoneBookRepository = phoneBookRepository;
            _entryRepository = entryRepository;
        }

        public IList<PhoneBook> GetPhoneBooks()
        {
            return _phoneBookRepository.GetAll();
        }

        public IList<Entry> GetPhoneBookEntries(int id)
        {
            return _entryRepository.Fetch(a => a.PhoneBookId == id);
        }

        public IList<Entry> FindPhoneBookEntries(int id, string query)
        {
            var entries = _entryRepository.Table.Where(a => a.PhoneBookId == id);
            if (!string.IsNullOrWhiteSpace(query))
            {
                entries = entries.Where(a => a.Name.Contains(query) || a.PhoneNumber.Contains(query));
            }
            return entries.ToList();
        }

        public PhoneBook GetPhoneBookById(int id)
        {
            return _phoneBookRepository.Get(a => a.Id == id);
        }

        public void CreatePhoneBook(PhoneBook phoneBook)
        {
            phoneBook.DateModified = phoneBook.DateModified ?? DateTime.Now;
            phoneBook.RowGuid = phoneBook.RowGuid ?? Guid.NewGuid();
            _phoneBookRepository.Create(phoneBook);
        }

        public void UpdatePhoneBook(PhoneBook phoneBook)
        {
            phoneBook.DateModified = phoneBook.DateModified ?? DateTime.Now;
            phoneBook.RowGuid = phoneBook.RowGuid ?? Guid.NewGuid();
            _phoneBookRepository.Update(phoneBook);
        }

        public void DeletePhoneBook(int id)
        {
            var phoneBook = _phoneBookRepository.Get(id);
            if (phoneBook != null)
            {
                foreach (var entry in phoneBook.Entries.ToList())
                {
                    _entryRepository.Delete(entry);
                }
            }
            _phoneBookRepository.Delete(id);
        }
    }
}