using System.Data.Entity;

namespace PhoneBookApp.Data.Entities
{
    public class PhoneBookContext : DbContext
    {
        public PhoneBookContext()
            : base("name=PhoneBookContext")
        {
        }

        public virtual DbSet<Entry> Entries { get; set; }
        public virtual DbSet<PhoneBook> PhoneBooks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entry>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Entry>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<PhoneBook>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}