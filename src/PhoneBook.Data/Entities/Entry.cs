using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhoneBookApp.Data.Entities
{
    [Table("Entry")]
    public class Entry
    {
        public int Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? DateModified { get; set; }

        public int? PhoneBookId { get; set; }

        public Guid? RowGuid { get; set; }

        public virtual PhoneBook PhoneBook { get; set; }
    }
}