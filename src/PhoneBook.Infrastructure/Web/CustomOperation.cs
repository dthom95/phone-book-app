﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;


namespace PhoneBookApp.Infrastructure.Web
{
    public class CustomOperation : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            ControllerActionDescriptor actionDescriptor = (ControllerActionDescriptor)context.ApiDescription.ActionDescriptor;
            operation.OperationId = actionDescriptor.ActionName;
        }
    }
}
