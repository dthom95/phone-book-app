﻿using System.Data.Entity;
using Autofac;
using PhoneBookApp.Common.Data;
using PhoneBookApp.Common.Data.EntityFramework;
using PhoneBookApp.Data.Entities;
using PhoneBookApp.Data.Services;

namespace PhoneBookApp.Infrastructure.Configuration
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {    
            builder.RegisterType<PhoneBookContext>().As<DbContext>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            // register services
            builder.RegisterType<PhoneBookService>().As<IPhoneBookService>().InstancePerLifetimeScope();
            builder.RegisterType<EntryService>().As<IEntryService>().InstancePerLifetimeScope();
        }        
    }
}