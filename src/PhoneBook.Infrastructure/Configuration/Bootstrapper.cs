﻿using Autofac;
using Autofac.Extras.CommonServiceLocator;
using CommonServiceLocator;

namespace PhoneBookApp.Infrastructure.Configuration
{
    public static class Bootstrapper
    {        
        public static IContainer Initialize(ContainerBuilder builder)
        {
            // register modules            
            builder.RegisterModule<DataModule>();

            // Perform registrations and build the container.
            var container = builder.Build();                                    

            // initialize service locator                  
            IServiceLocator serviceLocator = new AutofacServiceLocator(container);
            ServiceLocator.SetLocatorProvider(() => serviceLocator);

            return container;
        }
    }
}