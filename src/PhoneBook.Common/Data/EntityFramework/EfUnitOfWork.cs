﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Transactions;

namespace PhoneBookApp.Common.Data.EntityFramework
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private bool _disposed;

        public EfUnitOfWork(DbContext dbContext)
        {
            DbContext = dbContext;
        }

        public DbContext DbContext { get; }

        public void Commit()
        {            
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = IsolationLevel.ReadCommitted
            };
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                transactionOptions))
            {
                SaveChanges();
                scope.Complete();
            }
        }

        public void SaveChanges()
        {
           DbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
           await DbContext.SaveChangesAsync();
        }        

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }        

        #endregion        

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DbContext.Dispose();
                }
            }
            _disposed = true;
        }

        #endregion
    }
}
