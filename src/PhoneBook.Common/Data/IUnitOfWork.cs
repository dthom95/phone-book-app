﻿using System;
using System.Threading.Tasks;

namespace PhoneBookApp.Common.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}