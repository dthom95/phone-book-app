using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace PhoneBookApp.Common.Data
{
    public interface IRepository<T> {        
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
        void Delete(T entity);
        void Delete(Expression<Func<T, bool>> predicate);
        T Get(Expression<Func<T, bool>> predicate);         
        T Get(int id);
        List<T> GetAll();
        IQueryable<T> Table { get; }                

        int Count(Expression<Func<T, bool>> predicate);
        List<T> Fetch(Expression<Func<T, bool>> predicate);
        List<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order);
        List<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count);        
    }
}