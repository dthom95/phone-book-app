using AutoMapper;
using PhoneBookApp.Data.Entities;
using PhoneBookApp.Web.Models;

namespace PhoneBookApp.Web.Infrastructure
{
  public class WebMapper : Profile
  {
    public WebMapper()
    {
      CreateMap<EntryModel, Entry>()
        .ForMember(dest => dest.PhoneBook, opt => opt.Ignore());
      CreateMap<Entry, EntryModel>();

      CreateMap<PhoneBookModel, PhoneBook>()
        .ForMember(dest => dest.Entries, opt => opt.Ignore());
      CreateMap<PhoneBook, PhoneBookModel>();
    }
  }
}
