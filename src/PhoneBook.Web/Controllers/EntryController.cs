using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneBookApp.Common.Data;
using PhoneBookApp.Data.Entities;
using PhoneBookApp.Data.Services;
using PhoneBookApp.Web.Models;

namespace PhoneBookApp.Web.Controllers
{
  [Produces("application/json")]
  [Route("api/Entry")]
  public class EntryController : Controller
  {
    private readonly IMapper _mapper;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IEntryService _entryService;

    public EntryController(IMapper mapper, IUnitOfWork unitOfWork, IEntryService entryService)
    {
      _mapper = mapper;
      _unitOfWork = unitOfWork;
      _entryService = entryService;
    }

    [HttpGet]
    public IList<EntryModel> GetEntries()
    {
      var entries = _entryService.GetEntries();
      return _mapper.Map<List<EntryModel>>(entries);
    }

    [HttpGet("{id}")]
    public EntryModel GetEntryById(int id)
    {
      var phoneBook = _entryService.GetEntryById(id);
      return _mapper.Map<EntryModel>(phoneBook);
    }

    [HttpPost]
    public void CreateEntry([FromBody]EntryModel model)
    {
      var phoneBook = _mapper.Map<Entry>(model);
      _entryService.CreateEntry(phoneBook);
      _unitOfWork.Commit();
    }

    [HttpPut]
    public void UpdateEntry([FromBody]EntryModel model)
    {
      var phoneBook = _entryService.GetEntryById(model.Id);
      _mapper.Map(model, phoneBook);
      _entryService.UpdateEntry(phoneBook);
      _unitOfWork.Commit();
    }

    [HttpDelete("{id}")]
    public void DeleteEntry(int id)
    {
      _entryService.DeleteEntry(id);
      _unitOfWork.Commit();
    }
  }
}
