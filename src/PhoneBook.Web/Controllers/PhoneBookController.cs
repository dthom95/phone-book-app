using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneBookApp.Common.Data;
using PhoneBookApp.Data.Entities;
using PhoneBookApp.Data.Services;
using PhoneBookApp.Web.Models;

namespace PhoneBookApp.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/PhoneBook")]
    public class PhoneBookController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPhoneBookService _phoneBookService;

        public PhoneBookController(IMapper mapper, IUnitOfWork unitOfWork, IPhoneBookService phoneBookService)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _phoneBookService = phoneBookService;
        }

        [HttpGet]
        public IList<PhoneBookModel> GetPhoneBooks()
        {
            var phoneBooks = _phoneBookService.GetPhoneBooks();
            return _mapper.Map<List<PhoneBookModel>>(phoneBooks);
        }

        [HttpGet("{id}")]
        public PhoneBookModel GetPhoneBookById(int id)
        {
            var phoneBook = _phoneBookService.GetPhoneBookById(id);
            return _mapper.Map<PhoneBookModel>(phoneBook);
        }


        [HttpGet("entries/{id}")]
        public IList<EntryModel> GetPhoneBookEntries(int id)
        {
            var entries = _phoneBookService.GetPhoneBookEntries(id);
            return _mapper.Map<List<EntryModel>>(entries);
        }

        [HttpPost("entries/search")]
        public IList<EntryModel> FindPhoneBookEntries([FromBody]EntrySearchModel model)
        {
            var entries = _phoneBookService.FindPhoneBookEntries(model.PhoneBookId, model.Query);
            return _mapper.Map<List<EntryModel>>(entries);
        }

        [HttpPost]
        public void CreatePhoneBook([FromBody]PhoneBookModel model)
        {
            var phoneBook = _mapper.Map<PhoneBook>(model);
            _phoneBookService.CreatePhoneBook(phoneBook);
            _unitOfWork.Commit();
        }

        [HttpPut]
        public void UpdatePhoneBook([FromBody]PhoneBookModel model)
        {
            var phoneBook = _phoneBookService.GetPhoneBookById(model.Id);
            _mapper.Map(model, phoneBook);
            _phoneBookService.UpdatePhoneBook(phoneBook);
            _unitOfWork.Commit();
        }

        [HttpDelete("{id}")]
        public void DeletePhoneBook(int id)
        {
            _phoneBookService.DeletePhoneBook(id);
            _unitOfWork.Commit();
        }
    }
}
