namespace PhoneBookApp.Web.Models
{
    public class PhoneBookModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
