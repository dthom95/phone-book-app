﻿namespace PhoneBookApp.Web.Models
{
    public class EntrySearchModel
    {
        public int PhoneBookId { get; set; }
        public string Query { get; set; }
    }
}
