namespace PhoneBookApp.Web.Models
{
    public class EntryModel
    {
        public int Id { get; set; }
        public int? PhoneBookId { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
