import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EntryModel } from '../../../core/models/models';
import { PhoneBookService, EntryService } from '../../../core/services/services';
import { ItemService, NotificationService } from '../../../core/utils/utils';

import bootbox from 'bootbox.js/bootbox';

@Component({
  selector: 'app-entry-list',
  templateUrl: './entry-list.component.html',
  styleUrls: ['./entry-list.component.css']
})
export class EntryListComponent implements OnInit {
  phoneBookId: number;
  entries: EntryModel[];

  constructor(private readonly route: ActivatedRoute,
    private readonly itemService: ItemService,
    private readonly notificationService: NotificationService,
    private readonly phoneBookService: PhoneBookService,
    private readonly entryService: EntryService) {
    this.phoneBookId = 0;
    this.entries = [];
  }

  ngOnInit() {
    this.route.params.subscribe(param => {
      const phoneBookId = +param['phoneBookId'];
      if (phoneBookId > 0) {
        this.phoneBookId = phoneBookId;
        this.phoneBookService.getEntries(phoneBookId)
          .subscribe((entries: EntryModel[]) => {
            this.entries = entries;
          },
          (error) => {
            this.notificationService.error(`Error loading entries. ${error}`);
          });
      }
    });
  }

  onSearchEntry(query: string) {
    this.phoneBookService.findEntries(this.phoneBookId, query)
      .subscribe((entries: EntryModel[]) => {
        this.entries = entries;
      },
      (error) => {
        this.notificationService.error(`Error searching entries. ${error}`);
      });
  }

  onDeleteEntry(id: number) {
    bootbox.confirm('Are you sure you want to delete this item?', (result: any) => {
      if (result) {
        this.entryService.deleteEntry(id).subscribe(() => {
          const entry = this.entries.find(a => a.id === id);
          this.itemService.removeItemFromArray(this.entries, entry);
          this.notificationService.success('Entry successfully deleted.');
        }, (error) => {
          this.notificationService.error(`Error deleting entry. ${error}`);
        });
      }
      return true;
    });
  }
}
