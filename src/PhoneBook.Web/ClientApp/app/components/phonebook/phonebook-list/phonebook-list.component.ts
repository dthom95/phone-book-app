import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhoneBookModel } from '../../../core/models/PhoneBookModel';
import { PhoneBookService } from '../../../core/services/phonebook.service';
import { ItemService } from '../../../core/utils/item.service';
import { NotificationService } from '../../../core/utils/notification.service';
import bootbox from 'bootbox.js/bootbox';

@Component({
  selector: 'app-phonebook-list',
  templateUrl: './phonebook-list.component.html',
  styleUrls: ['./phonebook-list.component.css']
})
export class PhoneBookListComponent implements OnInit {
  phoneBooks: PhoneBookModel[];

  constructor(
    private readonly router: Router,
    private readonly itemService: ItemService,
    private readonly notificationService: NotificationService,
    private readonly phoneBookService: PhoneBookService) {
  }

  ngOnInit() {
    this.phoneBookService.getPhoneBooks()
      .subscribe((phoneBooks: PhoneBookModel[]) => {
        this.phoneBooks = phoneBooks;
      });
  }

  onDeletePhoneBook(id: number) {
    bootbox.confirm('Are you sure you want to delete this item?', (result : any) => {
      if (result) {
        this.phoneBookService.deletePhoneBook(id).subscribe(() => {
          const phoneBook = this.phoneBooks.find(a => a.id === id);
          this.itemService.removeItemFromArray(this.phoneBooks, phoneBook);
          this.notificationService.success('PhoneBook successfully deleted.');
        }, (error) => {
          this.notificationService.error(`Error deleting phonebook. ${error}`);
        });
      }
      return true;
    });
  }
}
