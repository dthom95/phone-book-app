import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PhoneBookService } from '../../../core/services/services';
import { NotificationService } from '../../../core/utils/utils';
import { PhoneBookModel } from '../../../core/models/models';

@Component({
  selector: 'app-phonebook-edit',
  templateUrl: './phonebook-edit.component.html',
  styleUrls: ['./phonebook-edit.component.css']
})
export class PhoneBookEditComponent implements OnInit {
  phoneBook: PhoneBookModel;

  constructor(private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly phoneBookService: PhoneBookService) {
    this.phoneBook = {};
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const phoneBookId = +params['phoneBookId'];
      if (phoneBookId > 0) {
        this.phoneBookService.getPhoneBookById(phoneBookId)
          .subscribe((phoneBook: PhoneBookModel) => {
            this.phoneBook = phoneBook;
          });
      }
    });
  }

  onSubmit(editForm: NgForm) {
    if (!editForm.form.valid) {
      return false;
    }
    if (this.phoneBook.id && this.phoneBook.id > 0) {
      this.updatePhoneBook(this.phoneBook);
    } else {
      this.createPhoneBook(this.phoneBook);
    }
    return true;
  }

  updatePhoneBook(phoneBook: PhoneBookModel) {
    this.phoneBookService.updatePhoneBook(phoneBook).subscribe(() => {
      this.router.navigate(['/phonebook']).then(() => {
        this.notificationService.success('Phonebook successfully updated.');
      });
    }, (error) => {
      this.notificationService.error(`Error updating phonebook. ${error}`);
    });
  }

  createPhoneBook(phoneBook: PhoneBookModel) {
    this.phoneBookService.createPhoneBook(phoneBook).subscribe(() => {
      this.router.navigate(['/phonebook']).then(() => {
        this.notificationService.success('Phonebook successfully created.');
      });
    }, (error) => {
      this.notificationService.error(`Error creating phonebook. ${error}`);
    });
  }
}
