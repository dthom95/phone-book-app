import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PhoneBookRoutingModule } from './phonebook.routing';

import { PhoneBookListComponent } from './phonebook-list/phonebook-list.component';
import { PhoneBookEditComponent } from './phonebook-edit/phonebook-edit.component';
import { EntryListComponent } from './entry-list/entry-list.component';
import { EntryEditComponent } from './entry-edit/entry-edit.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PhoneBookRoutingModule
  ],
  declarations: [PhoneBookListComponent, PhoneBookEditComponent, EntryListComponent, EntryEditComponent],
  exports: [PhoneBookRoutingModule]
})
export class PhoneBookModule { }
