import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { EntryModel } from '../../../core/models/models';
import { NotificationService } from '../../../core/utils/utils';
import { EntryService } from '../../../core/services/services';

@Component({
  selector: 'app-entry-edit',
  templateUrl: './entry-edit.component.html',
  styleUrls: ['./entry-edit.component.css']
})
export class EntryEditComponent implements OnInit {
  entry: EntryModel;

  constructor(private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly entryService: EntryService) {
    this.entry = {};
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const phoneBookId = +params['phoneBookId'];
      const entryId = +params['entryId'];
      if (phoneBookId > 0) {
        this.entry.phoneBookId = phoneBookId;
      }
      if (entryId > 0) {
        this.entryService.getEntryById(entryId)
          .subscribe((entry: EntryModel) => {
            this.entry = entry;
          },
          (error) => {
            this.notificationService.error(`Failed to load entry. ${error}`);
          });
      }
    });
  }

  onSubmit(editForm: NgForm) {
    if (!editForm.form.valid) {
      return false;
    }
    if (this.entry.id && this.entry.id > 0) {
      this.updateEntry(this.entry);
    } else {
      this.createEntry(this.entry);
    }
    return true;
  }

  createEntry(entry: EntryModel) {
    this.entryService.createEntry(this.entry).subscribe(() => {
      const navigateUrl = `/phonebook/entries/list/${this.entry.phoneBookId}`;
      this.router.navigate([navigateUrl]).then(() => {
        this.notificationService.success('Entry successfully created.');
      });
    }, (error) => {
      this.notificationService.error(`Failed to create entry. ${error}`);
    });
  }

  updateEntry(entry: EntryModel) {
    this.entryService.updateEntry(this.entry).subscribe(() => {
      const navigateUrl = `/phonebook/entries/list/${this.entry.phoneBookId}`;
      this.router.navigate([navigateUrl]).then(() => {
        this.notificationService.success('Entry successfully updated.');
      });
    }, (error) => {
      this.notificationService.error(`Failed to update entry. ${error}`);
    });
  }
}
