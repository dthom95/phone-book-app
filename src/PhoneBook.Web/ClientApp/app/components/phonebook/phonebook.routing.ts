import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PhoneBookListComponent } from './phonebook-list/phonebook-list.component';
import { PhoneBookEditComponent } from './phonebook-edit/phonebook-edit.component';
import { EntryListComponent } from './entry-list/entry-list.component';
import { EntryEditComponent } from './entry-edit/entry-edit.component';

const childRoutes: Routes = [
  { path: 'phonebook', component: PhoneBookListComponent },
  { path: 'phonebook/create', component: PhoneBookEditComponent },
  { path: 'phonebook/edit/:phoneBookId', component: PhoneBookEditComponent },
  { path: 'phonebook/entries/list/:phoneBookId', component: EntryListComponent },
  { path: 'phonebook/entries/create/:phoneBookId', component: EntryEditComponent },
  { path: 'phonebook/entries/edit/:entryId', component: EntryEditComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(childRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PhoneBookRoutingModule { }
