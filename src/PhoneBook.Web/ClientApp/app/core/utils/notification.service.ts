import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class NotificationService {
  constructor(private readonly toastr: ToastrService) { }

  success(message: string) {
    this.toastr.success(message, 'Success', { timeOut: 1000});
  }

  warning(message: string) {
    this.toastr.warning(message, 'Warning', { timeOut: 3000});
  }

  info(message: string) {
    this.toastr.info(message, 'Info', { timeOut: 1000});
  }

  error(message: string) {
    this.toastr.error(message, 'Error', { timeOut: 3000});
  }

  remove(toastId: number) {
    this.toastr.remove(toastId);
  }

  clear() {
    this.toastr.clear();
  }
}
