import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {
  baseUrl: string;
  apiUrl: string;

  constructor() {
    this.baseUrl = '/';
    this.apiUrl = 'http://localhost:53570';
  }

  getBaseUrl() {
    return this.apiUrl;
  }

  getApiUrl() {
    return this.apiUrl;
  }
}
