import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { ConfigService } from './utils/config.service';
import { ItemService, NotificationService} from './utils/utils';
import { PhoneBookService, EntryService } from './services/services';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue : '/' },
    ConfigService,
    NotificationService,
    ItemService,
    PhoneBookService,
    EntryService
  ],
  declarations: []
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
