import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { PhoneBookModel, EntryModel } from '../models/models';
import { ConfigService } from '../utils/config.service';

@Injectable()
export class PhoneBookService {

  _apiUrl = '';

  constructor(private readonly http: Http,
    private readonly configService: ConfigService) {
    this._apiUrl = configService.getApiUrl();
  }

  getPhoneBooks(): Observable<PhoneBookModel[]> {
    return this.http.get(`${this._apiUrl}/api/phonebook`)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getEntries(id: number): Observable<EntryModel[]> {
    return this.http.get(`${this._apiUrl}/api/phonebook/entries/${id}`)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  findEntries(id: number, query: string): Observable<EntryModel[]> {
    const data = {
      phoneBookId: id,
      query: query
    };
    return this.http.post(`${this._apiUrl}/api/phonebook/entries/search`, data)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  getPhoneBookById(id: number): Observable<PhoneBookModel> {
    return this.http.get(`${this._apiUrl}/api/phonebook/${id}`)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createPhoneBook(phoneBook: PhoneBookModel): Observable<PhoneBookModel> {
    return this.http.post(`${this._apiUrl}/api/phonebook`, phoneBook)
      .catch(this.handleError);
  }

  updatePhoneBook(phoneBook: PhoneBookModel): Observable<PhoneBookModel> {
    return this.http.put(`${this._apiUrl}/api/phonebook`, phoneBook)
      .catch(this.handleError);
  }

  deletePhoneBook(id: number) {
    return this.http.delete(`${this._apiUrl}/api/phonebook/${id}`)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
}
