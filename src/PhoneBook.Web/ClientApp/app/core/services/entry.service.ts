import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { EntryModel } from '../models/models';
import { ConfigService } from '../utils/config.service';

@Injectable()
export class EntryService {

  _apiUrl = '';

  constructor(private http: Http,
    private configService: ConfigService) {
    this._apiUrl = configService.getApiUrl();
  }

  getEntryById(id: number): Observable<EntryModel> {
    return this.http.get(`${this._apiUrl}/api/entry/${id}`)
      .map((res: Response) => {
        return res.json();
      })
      .catch(this.handleError);
  }

  createEntry(entry: EntryModel) {
    return this.http.post(`${this._apiUrl}/api/entry`, entry)
      .catch(this.handleError);
  }

  updateEntry(entry: EntryModel) {
    return this.http.put(`${this._apiUrl}/api/entry`, entry)
      .catch(this.handleError);
  }

  deleteEntry(id: number) {
    return this.http.delete(`${this._apiUrl}/api/entry/${id}`)
      .catch(this.handleError);
  }

  private handleError(error: any) {
    return Observable.throw(error);
  }
}
