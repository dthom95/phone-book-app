export interface EntryModel {
  id?: number;
  phoneBookId?: number;
  name?: string;
  dateModified?: Date;
  rowGuid?: string;
}
