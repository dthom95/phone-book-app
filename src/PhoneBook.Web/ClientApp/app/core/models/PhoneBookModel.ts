export interface PhoneBookModel {
    id?: number;
    name?: string;
    dateModified?: Date;
    rowGuid?: string;
}
